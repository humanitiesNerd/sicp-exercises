;;so, this is the procedure that yelds a recursive process

(define (+ a b)
  (if (= a 0) 
      b 
      (+ (dec a) (inc b))))


;;Our operation, +, is a generalization of inc, isn't it ?
;;So, it should be equivalent if I substituite the expression "(inc b)"
;;with "(+ 1 b)"

;;let's see what happens

(define (my+ a b)
  (if (= a 0) 
      b 
      (my+ (- a 1) (my+ 1 b))))


(+ 4 5)
(+ (dec 4) (+ 1 5))
(+ 3 (+ (dec 1) (+ 1 5)))
(+ 3 (+ 0 (+ (dec 1) (+ 1 5)))
(+ 3 (+ 0 (+ 0 (+ (dec 1) (+ 1 5)))))
(+ 3 (+ 0 (+ 0 (+ 0 (+ 1 5)))))
(+ 3 (+ 0 (+ 0 (+ 0 (+ (dec 1) (+ 1 5))))))


;; this keeps growing the stack until it blows it and then leaves you with a lot of unfreed memory

;; yet, the Ackerman function called itself recursively

;; this gets me confused: how do I write a recursive function that doesn't blow up ?
