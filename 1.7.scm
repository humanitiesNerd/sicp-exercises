(define (square x)
  (* x x))

;; https://www.pastery.net/wtsgkd/

(define (average x y) 
 "my average"
  (/ (+ x y) 2))



(define (improved-guess previous-guess x)
  (average previous-guess (/ x previous-guess)))


;; this is the previous version
;;(define (good-enough? guess x)
;  (pk (< (abs (- (square guess) x)) 0.001)))


(define (good-enough? previous-guess current-guess)
  (< (abs (- current-guess previous-guess)) 0.001 ))


(define (sqrt-iter previous-guess x)
  (let ((current-guess (improved-guess previous-guess x)))
    (if (good-enough? previous-guess current-guess)
        current-guess
        (sqrt-iter  current-guess x))))

(define (my-sqrt x)
  (sqrt-iter 1.0 x))
