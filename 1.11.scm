(define (fib n)
  (cond ((= n 0) 0)
        ((= n 1) 1)
        (else (+ (fib (- n 1))
                 (fib (- n 2))))))

;;a <- a + b
;;b <- a


(define (fib-it n) 
  (define (fib-iter a b count)
    (if (= count 0)
        b
        (fib-iter (+ a b) a (- count 1))))
  
  (fib-iter 1 0 n))


;; the exercise asks to calculate a variation of the Fibonacci
;; function that depends on 3 precedents (rather than 2) and
;; appliies some calculations to 3 precedents (instead of none)
;; but because that is too difficult for me to do directly
;; I take an intermediate step: I calculate a variation that
;; has 3 precedents BUT it doesn't apply any calculation to the
;; precedents
;; it follows


(define (fib2 n)
  (cond ((= n 0) 0)
        ((= n 1) 1)
	((= n 2) 2)
        (else (+ (fib2 (- n 1))
                 (fib2 (- n 2))
		 (fib2 (- n 3))))))

;; a <- a + b + c
;; b <- a
;; c <- b

;; notice that the test in the if is count = 0
;; because of this, we retunr c instead of a (or b)
;; this means that we are calculating 2 iterations
;; more than those required, here
(define (fib2-it n) 
  (define (fib2-iter a b c count)
    (if (= count 0)
        c
        (fib2-iter (+ a b c) a b  (- count 1))))
  
  (fib2-iter 2 1 0 n))


;; now let's add the calculations to 2 precedents


(define (f n)
  (if (< n 3)
      n
      (+ (f (- n 1))
	 (* 2 (f (- n 2)))
	 (* 3 (f (- n 3))))))



;; in the iterative version, this time, we try a better test
;; and in fact we return a instead of c
(define (f-it-iter a b c count)
 
  (if (< count 3)
      a
      (f-it-iter (+ a (* 2 b) (* 3 c)) a b (- count 1))))


(define (f-it n)
  (f-it-iter 2 1 0 n))
