(define (square x)
  (* x x))


;; this is a new version
(define (improve guess x)
  (/ (+ (/ x (square guess)) (* guess 2) ) 3))


(define (good-enough? previous-guess current-guess)
  (< (abs (- current-guess previous-guess)) 0.001 ))


(define (sqrt-iter previous-guess x)
  (let ((current-guess (improve previous-guess x)))
    (if (good-enough? previous-guess current-guess)
        current-guess
        (sqrt-iter  current-guess x))))



(define (my-sqrt x)
  (sqrt-iter 1.0 x))
